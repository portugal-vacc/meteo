from pydantic import BaseSettings


class Settings(BaseSettings):
    debug: bool = False
    server_ip: str = "0.0.0.0"

    class Config:
        env_file = ".env"
        env_file_encoding = "utf-8"


settings = Settings()
