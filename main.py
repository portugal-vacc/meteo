import asyncio

from hypercorn import Config
from hypercorn.asyncio import serve

from settings import settings


def run_api() -> None:
    from src.api.main import app

    config = Config()
    config.bind = [f"{settings.server_ip}:5000"]
    asyncio.run(serve(app, config))


def main() -> None:
    run_api()


if __name__ == "__main__":
    main()
