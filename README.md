# Meteo API

This repository contains Python code for an aviation weather API that provides METAR and TAF data for various airports. It also includes functionality for parsing and processing aviation weather data.

# Getting Started
## Docker

The container can be found in the registry

## Usage
### Endpoints

The API exposes the following endpoints:
    
```
/api/v1/airport - Airport Data
/api/v1/custom/metar - Parsed Custom Metar
/api/v1/custom/airport - Parsed Custom Airport

/api/v1/ipma/metar/raw - Raw Metar Ipma
/api/v1/ipma/metar - Parsed Metar Ipma
/api/v1/ipma/metar/all - Parsed All Metar Ipma
/api/v1/ipma/metar/decoded/text - Text Decoded Metar Ipma
/api/v1/ipma/taf/decoded/text - Text Decoded Taf Ipma
/api/v1/ipma/metar/topsky - Metar For Topsky From Ipma

/api/v1/ias_sure/{fir_code} - Fir Forecast
```

# Contributing

Contributions to this project are welcome. To contribute, follow these steps:

    Create a new branch for your feature or bug fix.
    Make your changes and commit them.
    Push the changes.
    Open a pull request from your fork to the main repository.

# License

This project is licensed under the MIT License - see the LICENSE file for details.