import httpx


def get_runways(airport_icao_codes: str) -> dict:
    """
    Retrieve runway information for the specified airport ICAO code(s) from an external API.

    Args:
        airport_icao_codes (str): A string containing one or more airport ICAO codes separated by commas.

    Returns:
        dict: A dictionary containing runway information for the specified airport(s). The keys are airport
         ICAO codes, and the values are lists of runway data.

    Raises:
        HTTPError: If an HTTP error occurs while making the API request.
    """
    response = httpx.get(f"https://navdata.vatsim.pt/api/runways/{airport_icao_codes}")
    response.raise_for_status()
    return [] if response.status_code == 404 else response.json()


if __name__ == "__main__":
    print(get_runways("lppt,lpcs"))
