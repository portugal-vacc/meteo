import math
from typing import Optional

from src.api.v1.airport.lowvis import lvo, smart_runway_selection
from src.propertybasemodel import PropertyBaseModel
from src.schemas import Metar


class WindComponent(PropertyBaseModel):
    """
    Represents wind component information for a specific runway.

    This class provides properties and calculations for wind components, including angle difference, head wind,
    and crosswind, based on the runway, wind speed, and wind direction.

    Args:
        runway (str): The identifier of the runway.
        wind_speed (int): The wind speed in knots.
        wind_direction (int): The wind direction in degrees.

    Attributes:
        runway (str): The identifier of the runway.
        wind_speed (int): The wind speed in knots.
        wind_direction (int): The wind direction in degrees.

    Properties:
        angle_difference (int): The angle difference between the wind direction and the runway direction.
        head_wind: The headwind component of the wind.
        cross_wind: The crosswind component of the wind.
    """

    runway: str
    wind_speed: int
    wind_direction: int

    @property
    def angle_difference(self) -> int:
        """
        Calculate the angle difference between wind direction and runway direction.

        Returns:
            int: The angle difference in degrees.
        """
        return self.wind_direction - int(f"{self.runway:<03}")

    @property
    def head_wind(self):
        """
        Calculate the headwind component of the wind.

        Returns:
            float: The headwind component in knots.
        """
        return self.wind_speed * math.cos(math.radians(self.angle_difference))

    @property
    def cross_wind(self):
        """
        Calculate the crosswind component of the wind.

        Returns:
            float: The crosswind component in knots.
        """
        return self.wind_speed * math.sin(math.radians(self.angle_difference))


class Airport(PropertyBaseModel):
    """
    Represents an airport with associated METAR information and runways.

    This class provides properties for determining optimal runways based on METAR conditions, checking for low visibility
    operations (LVO), and calculating wind components for each runway.

    Args:
        icao (str): The ICAO code of the airport.
        metar (Metar, optional): The METAR information for the airport. Default is None.
        runways (list, optional): A list of runway identifiers at the airport. Default is None.

    Attributes:
        icao (str): The ICAO code of the airport.
        metar (Metar, optional): The METAR information for the airport.
        runways (list, optional): A list of runway identifiers at the airport.

    Properties:
        optimal_runways (list[str]): The optimal runways for current conditions, if available.
        low_visibility_ops (bool): Indicates whether the airport is operating under low visibility conditions.
        wind_components (list[Optional[WindComponent]]): Wind components for each runway, if applicable.
    """

    icao: str
    metar: Optional[Metar]
    runways: Optional[list]

    @property
    def optimal_runways(self) -> list[str]:
        """
        Determine the optimal runways for current conditions, if available.

        Returns:
            list[str]: A list of optimal runway identifiers.
        """
        preferred_runway = None
        if (
            self.metar.has_visibility
            and self.metar.has_runway_visual_range
            and self.metar.has_clouds
        ):
            preferred_runway = smart_runway_selection(
                icao=self.icao,
                low_vis=self.low_visibility_ops,
                visibility=self.metar.visibility,
                runway_visual_range=self.metar.runway_visual_range,
                clouds=self.metar.clouds,
            )
        if preferred_runway:
            return [preferred_runway]
        optimal_runways = []
        for runway in self.runways:
            if self.metar.surface_wind.variable:
                optimal_runways.append(runway)
                continue
            if (
                next(
                    filter(lambda item: item.runway == runway, self.wind_components)
                ).head_wind
                >= 0
            ):
                optimal_runways.append(runway)
        return optimal_runways

    @property
    def low_visibility_ops(self) -> bool:
        """
        Check if the airport is operating under low visibility conditions (LVO).

        Returns:
            bool: True if the airport is operating under LVO, False otherwise.
        """
        if (
            self.metar.has_visibility
            and self.metar.has_runway_visual_range
            and self.metar.has_clouds
        ):
            return lvo(
                icao=self.icao,
                visibility=self.metar.visibility,
                runway_visual_range=self.metar.runway_visual_range,
                clouds=self.metar.clouds,
            )
        return False

    @property
    def wind_components(self) -> list[Optional[WindComponent]]:
        """
        Calculate wind components for each runway.

        Returns:
            list[Optional[WindComponent]]: A list of WindComponent instances for each applicable runway.
        """
        return [
            WindComponent(
                runway=runway,
                wind_speed=self.metar.surface_wind.wind_speed,
                wind_direction=self.metar.surface_wind.wind_direction,
            )
            for runway in self.runways
            if not self.metar.surface_wind.variable
        ]
