from typing import Optional

from src.schemas import MetarVisibility, MetarRunwayVisualRange, MetarClouds


def lvo(
    icao: str,
    visibility: Optional[MetarVisibility],
    runway_visual_range: list[Optional[MetarRunwayVisualRange]],
    clouds: list[Optional[MetarClouds]],
) -> bool:
    """
    Determine if Low Visibility Operations (LVO) conditions are met for a specific airport.

    This function evaluates the METAR data and cloud cover conditions to
     determine if Low Visibility Operations (LVO)
    conditions are met for a given airport identified by its ICAO code.

    Args:
        icao (str): The ICAO code of the airport.
        visibility (Optional[MetarVisibility]): The prevailing visibility data.
        runway_visual_range (list[Optional[MetarRunwayVisualRange]]): A list of runway visual range data.
        clouds (list[Optional[MetarClouds]]): A list of cloud cover data.

    Returns:
        bool: True if LVO conditions are met, False otherwise.
    """
    if icao.upper() == "LPFR":
        rvr_28 = [
            rvr
            for rvr in runway_visual_range
            if rvr.runway == "28" and int(rvr.value) <= 800
        ]
        cloud_limit = [
            cloud
            for cloud in clouds
            if cloud.cloud_amount in ["BKN", "OVC"] and int(cloud.height_of_base) <= 200
        ]
        return bool(rvr_28 or cloud_limit)

    if icao.upper() == "LPPT":
        rvr_20 = [
            rvr
            for rvr in runway_visual_range
            if rvr.runway == "20" and int(rvr.value) <= 550
        ]
        rvr_02 = [
            rvr
            for rvr in runway_visual_range
            if rvr.runway == "02" and int(rvr.value) <= 800
        ]
        cloud_limit = [
            cloud
            for cloud in clouds
            if cloud.cloud_amount in ["BKN", "OVC"] and int(cloud.height_of_base) <= 200
        ]
        return bool(rvr_20 or rvr_02 or cloud_limit)

    if icao.upper() == "LPMA":
        try:
            vis = visibility.prevailing_visibility <= 5000
        except (AttributeError, TypeError):
            vis = None
        cloud_limit = [
            cloud
            for cloud in clouds
            if cloud.cloud_amount in ["BKN", "OVC"] and int(cloud.height_of_base) <= 800
        ]
        return bool(vis or cloud_limit)

    if icao.upper() == "LPPR":
        rvr_17 = [
            rvr
            for rvr in runway_visual_range
            if rvr.runway == "17" and int(rvr.value) <= 550
        ]
        cloud_limit = [
            cloud
            for cloud in clouds
            if cloud.cloud_amount in ["BKN", "OVC"] and int(cloud.height_of_base) <= 200
        ]
        return bool(rvr_17 or cloud_limit)

    return False


def smart_runway_selection(
    icao: str,
    low_vis: bool,
    visibility: Optional[MetarVisibility],
    runway_visual_range: list[Optional[MetarRunwayVisualRange]],
    clouds: list[Optional[MetarClouds]],
) -> str | None:
    """
    Determine the optimal runway selection based on weather conditions for a specific airport.

    This function evaluates the METAR data, including visibility, runway visual range, and cloud cover conditions,
    to determine the optimal runway selection for a given airport identified by its ICAO code under
     specific weather conditions.

    Args:
        icao (str): The ICAO code of the airport.
        low_vis (bool): Indicates if low visibility conditions are present.
        visibility (Optional[MetarVisibility]): The prevailing visibility data.
        runway_visual_range (list[Optional[MetarRunwayVisualRange]]): A list of runway visual range data.
        clouds (list[Optional[MetarClouds]]): A list of cloud cover data.

    Returns:
        str | None: The selected runway identifier (e.g., "28") if a suitable runway is found,
         or None if no suitable runway is available.
    """
    if icao.upper() == "LPFR":
        return "28" if low_vis else None

    if icao.upper() == "LPPT":
        return (
            str(sorted(list(runway_visual_range), key=lambda rvr: rvr.value)[-1].runway)
            if low_vis and runway_visual_range
            else None
        )
    if icao.upper() == "LPMA":
        try:
            vis = visibility.prevailing_visibility <= 7000
        except (AttributeError, TypeError):
            vis = None
        cloud_limit = [
            cloud
            for cloud in clouds
            if cloud.cloud_amount in ["BKN", "OVC"]
            and int(cloud.height_of_base) <= 1200
        ]
        return "05" if low_vis or vis or cloud_limit else None

    if icao.upper() == "LPPR":
        try:
            vis = visibility.prevailing_visibility <= 2500
        except (AttributeError, TypeError):
            vis = None
        cloud_limit = [
            cloud
            for cloud in clouds
            if cloud.cloud_amount in ["BKN", "OVC"] and int(cloud.height_of_base) <= 400
        ]
        return "17" if low_vis or vis or cloud_limit else None
