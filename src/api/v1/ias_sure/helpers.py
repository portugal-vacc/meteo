import httpx


def pressure_to_flight_level(pressure: int) -> float:
    """
    Convert atmospheric pressure in hPa to flight level in feet.

    Parameters:
    - pressure (int): The atmospheric pressure in hPa (hectopascals).

    Returns:
    - float: The flight level in feet.
    """
    return int(((1 - (pressure / 1013.25) ** 0.190284) * 145366.45) / 100)


async def get_async(url):
    """
    Asynchronously perform an HTTP GET request to the specified URL.

    Parameters:
    - url (str): The URL to send the GET request to.

    Returns:
    - httpx.Response: A Response object representing the HTTP response.
    """
    async with httpx.AsyncClient() as client:
        return await client.get(url, timeout=None)
