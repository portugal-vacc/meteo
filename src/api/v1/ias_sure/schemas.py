import math
from datetime import datetime
from typing import Any, Dict, List, Optional

from pydantic import BaseModel


class MeteogramEcmwfHeader(BaseModel):
    """
    Represents the header information of a Meteogram from the ECMWF model.

    Attributes:
    - model (Any): The model information (type not specified).
    - refTime (datetime): The reference time for the forecast.
    - dsttime (Any): The destination time (type not specified).
    - update (datetime): The update time for the forecast.
    - dsttimeTs (Any): The destination time timestamp (type not specified).
    - step (Any): The step information (type not specified).
    - updateTs (Any): The update time timestamp (type not specified).
    - elevation (Any): The elevation information (type not specified).
    - modelElevation (Any): The model elevation information (type not specified).
    - cache (Any): Cache-related information (type not specified).
    """

    model: Any
    refTime: datetime
    dsttime: Any
    update: datetime
    dsttimeTs: Any
    step: Any
    updateTs: Any
    elevation: Any
    modelElevation: Any
    cache: Any


class WindyForecastMeteogramEcmwf(BaseModel):
    """
    Represents a Meteogram forecast from the ECMWF model on Windy.

    Attributes:
    - header (MeteogramEcmwfHeader): The header information of the Meteogram.
    - celestial (Any): Celestial information (type not specified).
    - data (Dict[str, List[Optional[float]]]): A dictionary of data values associated with specific keys.
      The values are lists of optional floats, and the keys represent data categories.
    """

    header: MeteogramEcmwfHeader
    celestial: Any
    data: Dict[str, List[Optional[float]]]


class MeteogramEcmwfDataParser(BaseModel):
    """
    Parses data from a Meteogram forecast from the ECMWF model on Windy.

    Attributes:
    - target_time (datetime): The target time for the data parsing.
    - data (Dict[str, List[Optional[float]]]): A dictionary of data values associated with specific keys.
      The values are lists of optional floats, and the keys represent data categories.

    Methods and Properties:
    - hours_as_datetime: Returns a list of human-readable datetime objects corresponding to the timestamps in the data.
    - get_data(key: str | int): Helper function to convert to the nomenclature used by Windy.
    - index_to_work_with: Determines the index of data to work with based on the closest timestamp to the target time.
    - temperature(pressure: int): Returns the temperature data for a specific pressure level.
    - wind_speed(pressure: int): Calculates and returns the wind speed for a specific pressure level.
    - wind_direction(pressure: int): Calculates and returns the wind direction for a specific pressure level.
    """

    target_time: datetime
    data: Dict[str, List[Optional[float]]]

    @property
    def hours_as_datetime(self) -> List:
        """Converts timestamps to human-readable dates."""
        return [
            datetime.fromtimestamp(timestamp / 1000.0)
            for timestamp in self.data.get("hours")
        ]

    def get_data(self, key: str | int):
        """Converts data keys to Windy's nomenclature."""
        if "1013h" in key:
            key = key.replace("1013h", "surface")
        return self.data.get(key)

    @property
    def index_to_work_with(self) -> int:
        """Determines the index of data to work with based on the closest timestamp to the target time."""
        res = min(
            self.data.get("hours"),
            key=lambda input_list: abs(
                input_list - self.target_time.timestamp() * 1000
            ),
        )
        return self.data.get("hours").index(res)

    def temperature(self, pressure: int):
        """Returns the temperature data for a specific pressure level."""
        return self.get_data(f"temp-{pressure}h")[self.index_to_work_with]

    def wind_speed(self, pressure: int):
        """Calculates and returns the wind speed for a specific pressure level.
        Logic copied from another code, owner Matisse"""
        return (
            math.sqrt(
                self.get_data(f"wind_u-{pressure}h")[self.index_to_work_with] ** 2
                + self.get_data(f"wind_v-{pressure}h")[self.index_to_work_with] ** 2
            )
            * 1.94384449
        )

    def wind_direction(self, pressure: int):
        """Calculates and returns the wind direction for a specific pressure level.
        Logic copied from another code, owner Matisse"""
        return (
            math.atan2(
                self.get_data(f"wind_u-{pressure}h")[self.index_to_work_with],
                self.get_data(f"wind_v-{pressure}h")[self.index_to_work_with],
            )
            / math.pi
            * 180
            + 180 % 360
        )
