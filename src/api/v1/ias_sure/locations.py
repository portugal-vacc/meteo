from typing import List, Optional

from pydantic import BaseModel


class Coordinates(BaseModel):
    """
    Represents geographic coordinates with latitude and longitude.

    Attributes:
    - latitude (float): The latitude value in degrees.
    - longitude (float): The longitude value in degrees.
    """

    latitude: float
    longitude: float


class Location(BaseModel):
    """
    Represents a named location with associated coordinates.

    Attributes:
    - name (str): The name of the location.
    - coordinates (Coordinates): The geographic coordinates of the location.
    """

    name: str
    coordinates: Coordinates


class FlightInformationRegion(BaseModel):
    """
    Represents a Flight Information Region (FIR) with a name and a list of locations.

    Attributes:
    - name (str): The name of the FIR.
    - locations (List[Optional[Location]]): A list of optional Location objects within the FIR.
      Some locations within the FIR may be None.
    """

    name: str
    locations: List[Optional[Location]]


FLIGHT_INFORMATION_REGIONS = {
    "LPPC": FlightInformationRegion(
        name="LPPC",
        locations=[
            Location(
                name="LPPR", coordinates=Coordinates(latitude=41.2355, longitude=-8.678)
            ),
            Location(
                name="VNCOA",
                coordinates=Coordinates(
                    latitude=41.219166666667, longitude=-7.3113333333333
                ),
            ),
            Location(
                name="FNDAO", coordinates=Coordinates(latitude=40.052, longitude=-7.663)
            ),
            Location(
                name="LEIRI",
                coordinates=Coordinates(latitude=39.833, longitude=-9.0143333333333),
            ),
            Location(
                name="CUBA",
                coordinates=Coordinates(latitude=38.2115, longitude=-7.9266666666667),
            ),
            Location(
                name="LPPT",
                coordinates=Coordinates(
                    latitude=38.774166666667, longitude=-9.1341666666667
                ),
            ),
            Location(
                name="LPFR",
                coordinates=Coordinates(latitude=37.0145, longitude=-7.9658333333333),
            ),
            Location(
                name="TIGGI",
                coordinates=Coordinates(latitude=36.305333333333, longitude=-10.86),
            ),
            Location(
                name="ABLIS",
                coordinates=Coordinates(latitude=38.590333333333, longitude=-10.816),
            ),
            Location(
                name="DEMOS",
                coordinates=Coordinates(latitude=41.704833333333, longitude=-10.003),
            ),
            Location(
                name="ABOPO",
                coordinates=Coordinates(
                    latitude=41.304166666667, longitude=-12.043166666667
                ),
            ),
            Location(
                name="ABLIS1",
                coordinates=Coordinates(
                    latitude=38.627166666667, longitude=-12.394833333333
                ),
            ),
            Location(
                name="VABEM",
                coordinates=Coordinates(
                    latitude=36.237166666667, longitude=-13.647166666667
                ),
            ),
            Location(
                name="PEVAP",
                coordinates=Coordinates(latitude=34.5175, longitude=-15.163333333333),
            ),
            Location(
                name="LPMA",
                coordinates=Coordinates(latitude=32.694166666667, longitude=-16.778),
            ),
        ],
    )
}
