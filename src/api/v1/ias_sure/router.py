import asyncio
from datetime import datetime
from typing import Literal

import httpx
from fastapi import APIRouter

from src.api.v1.ias_sure.config import WINDY_PRESSURES
from src.api.v1.ias_sure.helpers import pressure_to_flight_level, get_async
from src.api.v1.ias_sure.locations import FLIGHT_INFORMATION_REGIONS
from src.api.v1.ias_sure.schemas import MeteogramEcmwfDataParser

ias_sure_router = APIRouter(prefix="/ias_sure", tags=["meteo:ias_sure"])


@ias_sure_router.get("/{fir_code}")
async def fir_forecast(fir_code: Literal["lppc"]):
    """
    Endpoint to retrieve FIR (Flight Information Region) forecast data for a specific FIR code.

    Args:
        fir_code (Literal["lppc"]): The FIR code for which to retrieve forecast data.

    Returns:
        dict: A dictionary containing forecast information for the specified FIR.
    Raises:
        NotImplementedError: If the specified FIR code is not found in the FLIGHT_INFORMATION_REGIONS.
    """
    flight_information_region = FLIGHT_INFORMATION_REGIONS.get(fir_code.upper())
    if not flight_information_region:
        raise NotImplementedError()

    output = {}
    windy_data_for_info = httpx.get(
        "https://node.windy.com/forecast/meteogram/ecmwf/38.72282513119962/-9.357876276969257"
    ).json()
    output["info"] = {
        "date": windy_data_for_info.get("header").get("refTime"),
        "datestring": "",
    }  # Nick said this isn't implemented on his side anyways

    output["data"] = {}
    urls = [
        f"https://node.windy.com/forecast/meteogram/ecmwf/{location.coordinates.latitude}/{location.coordinates.longitude}"
        for location in flight_information_region.locations
    ]

    resps = await asyncio.gather(*map(get_async, urls))

    data = {str(response.url).split("ecmwf/")[1]: response.json() for response in resps}

    for location in flight_information_region.locations:
        location_output = {}
        location_output["levels"] = {}
        location_output["coords"] = {
            "lat": f"{location.coordinates.latitude}",
            "long": f"{location.coordinates.longitude}",
        }

        windy_data = data.get(
            f"{location.coordinates.latitude}/{location.coordinates.longitude}"
        )

        parser = MeteogramEcmwfDataParser(
            target_time=datetime.utcnow(), data=windy_data.get("data")
        )

        for pressure in WINDY_PRESSURES:
            location_output["levels"][pressure_to_flight_level(pressure)] = {
                "T(K)": f"{parser.temperature(pressure)}",
                "windspeed": f"{parser.wind_speed(pressure)}",
                "windhdg": f"{parser.wind_direction(pressure)}",
            }
        output["data"][location.name] = location_output
    return output
