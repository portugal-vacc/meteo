from fastapi import APIRouter

from src.api.v1.ipma.services import get_text
from src.helpers import remove_html_tags, get_metars, get_tafs, parse_decoded_metar
from src.metar_parser import parse_metar
from src.schemas import Metar

ipma_api = APIRouter(prefix="/ipma", tags=["IPMA Meteo Source"])


@ipma_api.get("/metar/raw", response_model=dict[str, str])
def raw_metar_ipma(airport_icao_codes: str):
    """
    Endpoint to retrieve raw METAR data for specified airports.

    Args:
        airport_icao_codes (str): Comma-separated list of airport ICAO codes.

    Returns:
        dict[str, str]: A dictionary where keys are the first four characters of
                       the ICAO codes, and values are corresponding METAR strings.
    """
    airports = list(airport_icao_codes.split(","))
    return {metar[:4]: metar for metar in get_metars(airports)}


@ipma_api.get("/metar", response_model=list[Metar])
def parsed_metar_ipma(airport_icao_codes: str):
    """
    Endpoint to retrieve and parse METAR data for specified airports.

    Args:
        airport_icao_codes (str): Comma-separated list of airport ICAO codes.

    Returns:
        list[Metar]: A list of parsed METAR objects for the specified airports.
    """
    airports: list[str] = list(airport_icao_codes.split(","))
    return [parse_metar(metar) for metar in get_metars(airports)]


@ipma_api.get("/metar/all", response_model=list[Metar])
def parsed_all_metar_ipma():
    """
    Endpoint to retrieve and parse METAR data for a predefined list of airports.

    Returns:
        list[Metar]: A list of parsed METAR objects for the predefined airports.
    """
    airports: list[str] = [
        "lppt",
        "lppr",
        "lpfr",
        "lppd",
        "lpaz",
        "lpma",
        "lpps",
        "lphr",
        "lpfl",
        "lppi",
        "lpsj",
        "lpgr",
        "lpcr",
        "lpcs",
    ]
    return [parse_metar(metar) for metar in get_metars(airports)]


@ipma_api.get("/taf", response_model=list[dict[str, str]], include_in_schema=False)
def taf_ipma(airport_icao_codes: str):
    # TODO
    airports: list[str] = list(airport_icao_codes.split(","))
    return [match["content"] for match in get_tafs(airports)]


@ipma_api.get("/metar/decoded/text")
def text_decoded_metar_ipma(airport_icao_code: str):
    """
    Endpoint to retrieve and parse decoded METAR text data for a specific airport.

    Args:
        airport_icao_code (str): The ICAO code of the airport for which to retrieve and parse METAR data.

    Returns:
        List[str]: A list of parsed METAR strings for the specified airport.
    """
    text = get_text(
        airport_icao_code=airport_icao_code, content_type="metar", decoded=True
    )
    return parse_decoded_metar(text)


@ipma_api.get("/taf/decoded/text", response_model=list[list])
def text_decoded_taf_ipma(airport_icao_code: str):
    """
    Endpoint to retrieve and parse decoded TAF (Terminal Aerodrome Forecast) text data for a specific airport.

    Args:
        airport_icao_code (str): The ICAO code of the airport for which to retrieve and parse TAF data.

    Returns:
        List[list]: A list of parsed TAF data for the specified airport, with each item representing a section of
        the TAF report.
    """
    parts = get_text(
        airport_icao_code=airport_icao_code, content_type="taf", decoded=True
    ).split("---------------------------------------------------------------")
    return [remove_html_tags(part).replace("\n", "").split("  ") for part in parts]


@ipma_api.get("/metar/topsky", response_model=str)
def metar_for_topsky_from_ipma(airport_icao_codes: str):
    """
    Endpoint to retrieve METAR (Meteorological Aerodrome Report) data for multiple airports from IPMA and format it
    for TopSky integration.

    Args:
        airport_icao_codes (str): Comma-separated list of ICAO codes for the airports for which to retrieve METAR data.

    Returns:
        str: A formatted string containing METAR data for TopSky integration, with each METAR report enclosed in
        '(prefix)' and '(suffix)'.
    """
    airports: list[str] = list(airport_icao_codes.split(","))
    return "".join((f"(prefix){metar}(suffix)" for metar in get_metars(airports)))
