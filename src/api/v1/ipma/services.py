from typing import List, Literal

import httpx


def show_op_met_query(
    airport_icao_codes: List[str], content_type: Literal["metar", "taf"]
) -> str:
    """
    Retrieve operational METAR or TAF data for a list of airports.

    Args:
        airport_icao_codes (List[str]): A list of ICAO codes of airports for which to retrieve data.
        content_type (Literal["metar", "taf"]): The type of data to retrieve, either "metar" or "taf".

    Returns:
        str: The operational METAR or TAF data for the specified airports.

    Raises:
        HTTPError: If there is an issue with the HTTP request or response.
    """
    params = " ".join([code.lower() for code in airport_icao_codes])
    response = httpx.post(
        "https://brief-ng.ipma.pt/showopmetquery.php",
        data={"icaos": params, "type": content_type},
    )
    response.raise_for_status()
    return response.text


def get_text(
    airport_icao_code: str, content_type: Literal["metar", "taf"], decoded: bool = False
) -> str:
    """
    Retrieve METAR or TAF text data for a specific airport.

    Args:
        airport_icao_code (str): The ICAO code of the airport for which to retrieve data.
        content_type (Literal["metar", "taf"]): The type of data to retrieve, either "metar" or "taf".
        decoded (bool, optional): Whether to retrieve decoded data (True) or raw data (False). Defaults to False.

    Returns:
        str: The METAR or TAF text data for the specified airport.

    Raises:
        HTTPError: If there is an issue with the HTTP request or response.
    """
    response = httpx.get(
        f"https://brief-ng.ipma.pt/viewstation.php?icao={airport_icao_code.upper()}"
        f"&type={'FT' if content_type == 'taf' else 'SA'}{'&decoded=t' if decoded else ''}"
    )
    response.raise_for_status()
    return response.text
