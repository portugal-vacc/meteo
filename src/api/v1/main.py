from fastapi import APIRouter

from src.api.v1.airport.schemas import Airport
from src.api.v1.airport.services import get_runways
from src.api.v1.ias_sure.router import ias_sure_router
from src.api.v1.ipma.router import ipma_api
from src.helpers import get_metars
from src.metar_parser import parse_metar
from src.schemas import Metar

v1_api = APIRouter(prefix="/v1")

v1_api.include_router(ipma_api)
v1_api.include_router(ias_sure_router)


@v1_api.get("/airport", response_model=list[Airport])
def airport_data(airport_icao_codes: str):
    """
    Retrieve airport data including METAR and runway information.

    This route takes a comma-separated list of airport ICAO codes as input and retrieves data for each airport.
    It fetches METAR data for the specified airports and gathers runway information.

    Args:
        airport_icao_codes (str): A comma-separated list of airport ICAO codes to retrieve data for.

    Returns:
        List[Airport]: A list of `Airport` objects containing METAR and runway information for each airport.
    """
    airport_list = list(airport_icao_codes.split(","))
    metars = {
        match.identification.location: match
        for match in [parse_metar(metar) for metar in get_metars(airport_list)]
    }
    airports_runways = get_runways(airport_icao_codes)

    return [
        Airport(
            icao=airport_icao_code,
            metar=metars.get(airport_icao_code.upper()),
            runways=airports_runways.get(airport_icao_code.upper()),
        )
        for airport_icao_code in airport_list
    ]


@v1_api.get("/custom/metar", response_model=Metar)
def parsed_custom_metar(metar: str):
    """
    Parse a custom METAR string and return structured METAR data.

    This route takes a custom METAR string as input, parses it using the `parse_metar` function,
    and returns the structured METAR data.

    Args:
        metar (str): The custom METAR string to be parsed.

    Returns:
        Metar: An instance of the `Metar` class containing parsed METAR data.
    """
    return parse_metar(metar)


@v1_api.get("/custom/airport", response_model=Airport)
def parsed_custom_airport(metar: str):
    """
    Parse a custom METAR string, retrieve airport data, and return an `Airport` object.

    This route takes a custom METAR string as input, parses it using the `parse_metar` function,
    retrieves airport data including METAR and runway information, and returns an `Airport` object.

    Args:
        metar (str): The custom METAR string to be parsed.

    Returns:
        Airport: An `Airport` object containing METAR and runway information for the airport.
    """
    metar = parse_metar(metar)
    icao = metar.identification.location
    airports_runways = get_runways(icao)
    return Airport(icao=icao, metar=metar, runways=airports_runways.get(icao.upper()))
