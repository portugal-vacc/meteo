import re
from typing import Iterator, Match, List, Generator

from src.api.v1.ipma.services import show_op_met_query


def metar_parser_iterator(raw_data: str) -> Iterator[Match]:
    """
    Iterate over METAR (Meteorological Aerodrome Report) strings in raw data.

    Args:
        raw_data (str): The raw data containing METAR reports.

    Yields:
        Iterator[Match]: An iterator yielding Match objects for each found METAR report.
            Each Match object contains information about the report and its content.

    Example:
        raw_data = "METAR ABCD ... METAR EFGH ..."
        for match in metar_parser_iterator(raw_data):
            print(match.group('airport'), match.group('content'))
    """
    return re.finditer(r"METAR (?P<content>(?P<airport>\S{4}).*)", raw_data)


def get_metars(airport_icao_codes: List[str]) -> Generator:
    """
    Retrieve METAR (Meteorological Aerodrome Report) data for specified airports.

    Args:
        airport_icao_codes (List[str]): A list of ICAO airport codes for which METAR reports are to be retrieved.

    Yields:
        Generator[str, None, None]: A generator that yields METAR report content as strings.

    Example:
        airport_codes = ["ABCD", "EFGH"]
        metar_generator = get_metars(airport_codes)
        for metar_report in metar_generator:
            print(metar_report)
    """
    raw_data: str = show_op_met_query(
        airport_icao_codes=airport_icao_codes, content_type="metar"
    )
    return (metar["content"] for metar in metar_parser_iterator(raw_data))


def taf_parser_iterator(raw_data: str) -> Iterator[Match[str]]:
    # TODO
    return re.finditer(
        r"TAF (?P<content>(?P<airport>\S{4}) .*\n(?:.*BECMG| .*\n)*)", raw_data
    )


def remove_html_tags(raw_html) -> str:
    """
    Remove HTML tags from a given string.

    Args:
        raw_html (str): The input string containing HTML tags.

    Returns:
        str: A new string with all HTML tags removed.

    Example:
        html_string = "<p>This is <em>sample</em> text.</p>"
        clean_text = remove_html_tags(html_string)
        print(clean_text)  # Output: "This is sample text."
    """
    return re.sub(re.compile("<.*?>"), "", raw_html)


def get_tafs(airport_icao_codes: List[str]) -> List[Match]:
    # TODO
    raw_data: str = show_op_met_query(
        airport_icao_codes=airport_icao_codes, content_type="taf"
    )
    return list(taf_parser_iterator(raw_data))


def parse_decoded_metar(raw_text: str) -> list[str]:
    """
    Parse decoded METAR (Meteorological Aerodrome Report) text into a list of lines.

    Args:
        raw_text (str): The decoded METAR text to be parsed.

    Returns:
        list[str]: A list of strings, each representing a line of the parsed METAR text.

    Example:
        decoded_metar = "METAR ABCD 010100Z ..."
        parsed_lines = parse_decoded_metar(decoded_metar)
        for line in parsed_lines:
            print(line)
    """
    clean = remove_html_tags(raw_text)
    return clean.split("\n")
