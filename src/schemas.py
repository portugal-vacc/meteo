from datetime import datetime
from typing import Optional

from pydantic import BaseModel


class MetarIdentification(BaseModel):
    """
    Represents a METAR (Meteorological Aerodrome Report) identification data structure.

    Attributes:
        corrected (bool): Indicates whether this METAR report is a corrected version.
        location (str): The location or airport identifier for which the METAR report is issued.
        automatic (bool): Indicates whether the report is generated automatically by a weather station.
        issued (datetime): The date and time when the METAR report was issued.
        outdated (bool): Indicates whether the METAR report is outdated or superseded.
        refer_to_location (Optional[str]): An optional reference to a different location's METAR report.
    """

    corrected: bool = False
    location: str
    automatic: bool = False
    issued: datetime
    outdated: bool = False
    refer_to_location: Optional[str]


class MetarSurfaceWind(BaseModel):
    """
    Represents surface wind data in a METAR (Meteorological Aerodrome Report).

    Attributes:
        wind_direction (Optional[int]): The wind direction in degrees, if available.
        variable (bool): Indicates if the wind direction is variable.
        wind_speed (int): The wind speed in knots, if available.
        gust (bool): Indicates if gusty winds are reported.
        more_than_100kts (bool): Indicates if the wind speed is greater than 100 knots.
        maximum_wind_speed (Optional[int]): The maximum wind speed in knots, if available.
        unit (str): The unit of wind speed measurement (e.g., 'KT' for knots).
        wind_variable_from (Optional[int]): The starting direction of variable winds, if applicable.
        variability (bool): Indicates if wind variability is reported.
        wind_variable_to (Optional[int]): The ending direction of variable winds, if applicable.
    """

    wind_direction: Optional[int] = None
    variable: bool = False
    wind_speed: int = None
    gust: bool = False
    more_than_100kts: bool = False
    maximum_wind_speed: Optional[int] = None
    unit: str = None
    wind_variable_from: Optional[int] = None
    variability: bool = False
    wind_variable_to: Optional[int] = None


class MetarVisibility(BaseModel):
    """
    Represents visibility data in a METAR (Meteorological Aerodrome Report).

    Attributes:
        prevailing_visibility (Optional[int]): The prevailing visibility in meters, if available.
        lowest_visibility (Optional[int]): The lowest visibility in meters, if available.
        lowest_visibility_direction (Optional[int]): The direction associated with the lowest visibility, if available.
    """

    prevailing_visibility: Optional[int]
    lowest_visibility: Optional[int]
    lowest_visibility_direction: Optional[int]


class MetarRunwayVisualRange(BaseModel):
    """
    Represents runway visual range data in a METAR (Meteorological Aerodrome Report).

    Attributes:
        runway (str): The identifier of the runway.
        modifier (Optional[str]): A modifier for the visual range data, if available.
        value (int): The visual range value in meters.
        tendency (Optional[str]): The tendency of the visual range, if available.
    """

    runway: str
    modifier: Optional[str]
    value: int
    tendency: Optional[str]


class MetarPresentWeather(BaseModel):
    """
    Represents present weather conditions data in a METAR (Meteorological Aerodrome Report).

    Attributes:
        intensity (Optional[str]): The intensity of present weather, if available.
        descriptor (Optional[str]): Additional descriptor of present weather, if available.
        precipitation (Optional[str]): Type of precipitation, if available.
        obscuration (Optional[str]): Obscuration description, if available.
        other (Optional[str]): Other weather-related information, if available.
    """

    intensity: Optional[str]
    descriptor: Optional[str]
    precipitation: Optional[str]
    obscuration: Optional[str]
    other: Optional[str]


class MetarClouds(BaseModel):
    """
    Represents cloud cover data in a METAR (Meteorological Aerodrome Report).

    Attributes:
        cloud_amount (str): The amount or density of cloud cover.
        height_of_base (Optional[str]): The height of the cloud base, if available.
        cloud_type (Optional[str]): The type of cloud, if available.
    """

    cloud_amount: str
    height_of_base: Optional[str]
    cloud_type: Optional[str]


class MetarTemperatures(BaseModel):
    """
    Represents temperature data in a METAR (Meteorological Aerodrome Report).

    Attributes:
        air_temperature (int): The air temperature in degrees Celsius.
        dew_point_temperature (int): The dew point temperature in degrees Celsius.
    """

    air_temperature: int
    dew_point_temperature: int


class WindShear(BaseModel):
    """
    Represents wind shear data in a METAR (Meteorological Aerodrome Report).

    Attributes:
        runway (Optional[int]): The runway associated with wind shear, if available.
        all_runways (bool): Indicates if the wind shear affects all runways.
    """

    runway: Optional[int]
    all_runways: bool = False


class Metar(BaseModel):
    """
    Represents a METAR (Meteorological Aerodrome Report) weather data structure.

    Attributes:
        identification (MetarIdentification): Information about the METAR identification.
        surface_wind (MetarSurfaceWind): Information about surface wind conditions.
        visibility (Optional[MetarVisibility]): Information about visibility conditions, if available.
        runway_visual_range (list[Optional[MetarRunwayVisualRange]]): List of runway visual range data, if available.
        present_weather (list[Optional[MetarPresentWeather]]): List of present weather conditions, if available.
        clouds (list[Optional[MetarClouds]]): List of cloud cover data, if available.
        vertical_visibility (Optional[int]): Vertical visibility in meters, if available.
        cavok (bool): Indicates if the METAR report indicates CAVOK (Ceiling and Visibility OK) conditions.
        temperatures (MetarTemperatures): Information about air and dew point temperatures.
        pressure (int | str): Atmospheric pressure data (integer or string).
        wind_shear (list[Optional[WindShear]]): List of wind shear data, if available.
        trend_forecast (Optional[str]): Trend forecast information, if available.
        remarks (Optional[str]): Additional remarks or information, if available.
        raw (str): The raw METAR report string.

    Properties:
        has_visibility (bool): Indicates if visibility information is available.
        has_runway_visual_range (bool): Indicates if runway visual range information is available.
        has_present_weather (bool): Indicates if present weather information is available.
        has_clouds (bool): Indicates if cloud cover information is available.
    """

    identification: MetarIdentification
    surface_wind: MetarSurfaceWind
    visibility: Optional[MetarVisibility]
    runway_visual_range: list[Optional[MetarRunwayVisualRange]]
    present_weather: list[Optional[MetarPresentWeather]]
    clouds: list[Optional[MetarClouds]]
    vertical_visibility: Optional[int]
    cavok: bool = False
    temperatures: MetarTemperatures
    pressure: int | str
    wind_shear: list[Optional[WindShear]]
    trend_forecast: Optional[str]
    remarks: Optional[str]
    raw: str

    @property
    def has_visibility(self):
        """
        Check if visibility data is present in the METAR report.

        Returns:
            bool: True if visibility data is present, False otherwise.
        """
        return bool(self.visibility)

    @property
    def has_runway_visual_range(self):
        """
        Check if runway visual range data is present in the METAR report.

        Returns:
            bool: True if runway visual range data is present, False otherwise.
        """
        return bool(self.runway_visual_range)

    @property
    def has_present_weather(self):
        """
        Check if present weather data is present in the METAR report.

        Returns:
            bool: True if present weather data is present, False otherwise.
        """
        return bool(self.present_weather)

    @property
    def has_clouds(self):
        """
        Check if cloud cover data is present in the METAR report.

        Returns:
            bool: True if cloud cover data is present, False otherwise.
        """
        return bool(self.clouds)

    @property
    def meterological_condition(self) -> str:
        """
        Determines the meteorological condition based on prevailing visibility and cloud cover.

        Returns:
            str: Either "IMC" (Instrument Meteorological Conditions) if prevailing visibility is below 5000 meters
                 and there are broken or overcast clouds below 1500 meters, or "VMC" (Visual Meteorological Conditions)
                 otherwise.
        """
        return (
            "IMC"
            if self.visibility.prevailing_visibility < 5000
            and [
                cloud.cloud_amount in ["BKN", "OVC"] or cloud.height_of_base < 1500
                for cloud in self.clouds
            ]
            else "VMC"
        )
