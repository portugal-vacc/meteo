import logging
import sys

FORMATTER = logging.Formatter("%(asctime)s — %(name)s — %(levelname)s — %(message)s")


def get_console_handler():
    """
    Get a console handler for logging messages to the standard output (stdout).

    Returns:
        logging.StreamHandler: A console handler configured with the specified log format.

    Example:
        console_handler = get_console_handler()
        logger = logging.getLogger(__name__)
        logger.addHandler(console_handler)
        logger.setLevel(logging.INFO)
        logger.info("This is an informational message.")
    """
    console_handler = logging.StreamHandler(sys.stdout)
    console_handler.setFormatter(FORMATTER)
    return console_handler


def get_logger(logger_name):
    """
    Get a configured logger with the specified name.

    Args:
        logger_name (str): The name of the logger.

    Returns:
        logging.Logger: A logger instance configured for logging to the console.

    Example:
        logger = get_logger("my_logger")
        logger.debug("This is a debug message.")
        logger.info("This is an informational message.")
        logger.error("This is an error message.")
    """
    logger = logging.getLogger(logger_name)
    logger.setLevel(logging.DEBUG)
    logger.addHandler(get_console_handler())
    logger.propagate = False
    return logger
