from typing import Union

from pydantic import BaseModel


class PropertyBaseModel(BaseModel):
    """
    Workaround for serializing properties with pydantic until
    https://github.com/samuelcolvin/pydantic/issues/935
    is solved

    This class extends the functionality of pydantic's BaseModel to support serialization of properties.

    Methods:
        get_properties(): Get a list of property names defined in the class.
        dict(include, exclude, by_alias, skip_defaults, exclude_unset, exclude_defaults, exclude_none):
            Return the model's attribute dictionary including properties.
    """

    @classmethod
    def get_properties(cls):
        """
        Get a list of property names defined in the class.

        This method returns a list of property names defined in the class. It checks the class attributes
        to identify properties and filters out internal pydantic attributes ('__values__' and 'fields').

        Returns:
            List[str]: A list of property names in the class.
        """
        return [
            prop
            for prop in dir(cls)
            if isinstance(getattr(cls, prop), property)
            and prop not in ("__values__", "fields")
        ]

    def dict(
        self,
        *,
        include: Union["AbstractSetIntStr", "MappingIntStrAny"] = None,
        exclude: Union["AbstractSetIntStr", "MappingIntStrAny"] = None,
        by_alias: bool = False,
        skip_defaults: bool = None,
        exclude_unset: bool = False,
        exclude_defaults: bool = False,
        exclude_none: bool = False,
    ) -> "DictStrAny":
        """
        Generate a dictionary representation of the model's attributes including properties.

        Args:
            include (Union[AbstractSetIntStr, MappingIntStrAny], optional): Fields to include in the dictionary.
            exclude (Union[AbstractSetIntStr, MappingIntStrAny], optional): Fields to exclude from the dictionary.
            by_alias (bool, optional): Whether to use field aliases as keys in the dictionary.
            skip_defaults (bool, optional): Whether to skip fields with default values.
            exclude_unset (bool, optional): Whether to exclude fields that have not been explicitly set.
            exclude_defaults (bool, optional): Whether to exclude fields with default values.
            exclude_none (bool, optional): Whether to exclude fields with None values.

        Returns:
            DictStrAny: A dictionary representation of the model's attributes including properties.
        """
        attribs = super().dict(
            include=include,
            exclude=exclude,
            by_alias=by_alias,
            skip_defaults=skip_defaults,
            exclude_unset=exclude_unset,
            exclude_defaults=exclude_defaults,
            exclude_none=exclude_none,
        )
        props = self.get_properties()
        # Include and exclude properties
        if include:
            props = [prop for prop in props if prop in include]
        if exclude:
            props = [prop for prop in props if prop not in exclude]

        # Update the attribute dict with the properties
        if props:
            attribs.update({prop: getattr(self, prop) for prop in props})

        return attribs
