import re
from datetime import datetime, timedelta
from typing import Optional

from pydantic import BaseModel

from src.schemas import (
    MetarIdentification,
    MetarSurfaceWind,
    MetarRunwayVisualRange,
    MetarVisibility,
    MetarPresentWeather,
    MetarClouds,
    MetarTemperatures,
    Metar,
    WindShear,
)

REFERER_LOCATIONS = {"LPCS": "LPPT", "LPMT": "LPPT"}


class MetarParser(BaseModel):
    """
    Parses a METAR (Meteorological Aerodrome Report) weather data string into structured data.

    Args:
        metar (str): The raw METAR weather data string.

    Methods:
        parse_identification(): Parse METAR identification data.
        parse_surface_wind(): Parse surface wind data.
        parse_prevailing_visibility(): Parse prevailing visibility data.
        parse_runway_visual_range(): Parse runway visual range data.
        parse_present_weather(): Parse present weather conditions data.
        parse_clouds(): Parse cloud cover data.
        parse_vertical_visibility(): Parse vertical visibility data.
        parse_cavok(): Parse CAVOK (Ceiling and Visibility OK) condition.
        parse_air_and_dew_point_temperature(): Parse air and dew point temperature data.
        parse_pressure(): Parse atmospheric pressure data.
        parse_wind_shear(): Parse wind shear data.
        parse_trend_forecast(): Parse trend forecast data.

    Attributes:
        metar (str): The raw METAR weather data string to be parsed.
    """

    metar: str

    def parse_identification(self) -> MetarIdentification:
        """
        Parse METAR identification data from the raw METAR string.

        Returns:
            MetarIdentification: An instance of the `MetarIdentification` class containing identification data.
        """
        matches = re.search(
            r"(?P<icao>\w{4}) (?P<day>\d{2})(?P<hour>\d{2})(?P<minute>\d{2})Z",
            self.metar,
        )
        metar_datetime = datetime.utcnow().replace(
            day=int(matches.group("day")),
            hour=int(matches.group("hour")),
            minute=int(matches.group("minute")),
            second=0,
            microsecond=0,
        )
        outdated = datetime.utcnow() - metar_datetime > timedelta(hours=1, minutes=30)
        refer_to_location = (
            REFERER_LOCATIONS.get(matches.group("icao"))
            if outdated and matches.group("icao") in REFERER_LOCATIONS
            else None
        )
        return MetarIdentification(
            location=matches.group("icao"),
            corrected="COR" in self.metar,
            automatic="AUTO" in self.metar,
            issued=metar_datetime,
            outdated=outdated,
            refer_to_location=refer_to_location,
        )

    def parse_surface_wind(self) -> MetarSurfaceWind:
        """
        Parse surface wind data from the raw METAR string.

        Returns:
            MetarSurfaceWind: An instance of the `MetarSurfaceWind` class containing surface wind data.
        """
        matches = re.search(
            r"((?P<wind_direction>\d{3})|(?P<variable>VRB))(?P<more_than_100kts>P|ABV)?(?P<wind_intensity>\d{2,3})G?"
            r"(?P<maximum>\d{2})?(?P<unit>KT|MPS)( (?P<wind_variable_from>\d{3})V(?P<wind_variable_to>\d{3}))?",
            self.metar,
        )
        if not matches:
            return MetarSurfaceWind()
        return MetarSurfaceWind(
            wind_direction=matches.group("wind_direction"),
            variable=bool(matches.group("variable")),
            wind_speed=matches.group("wind_intensity"),
            gust=bool(matches.group("maximum")),
            more_than_100kts=bool(matches.group("more_than_100kts")),
            maximum_wind_speed=matches.group("maximum"),
            unit=matches.group("unit"),
            wind_variable_from=matches.group("wind_variable_from"),
            variability=bool(matches.group("wind_variable_to")),
            wind_variable_to=matches.group("wind_variable_to"),
        )

    def parse_prevailing_visibility(self) -> MetarVisibility:
        """
        Parse prevailing visibility data from the raw METAR string.

        Returns:
            MetarVisibility: An instance of the `MetarVisibility` class containing prevailing visibility data.
        """
        matches = re.search(r" (?P<prevailing_visibility>\d{4}) ", self.metar)
        return MetarVisibility(
            prevailing_visibility=(
                matches.group("prevailing_visibility") if matches else None
            ),
            lowest_visibility=None,
            lowest_visibility_direction=None,
        )

    def parse_runway_visual_range(self) -> list[Optional[MetarRunwayVisualRange]]:
        """
        Parse runway visual range data from the raw METAR string.

        Returns:
            list[Optional[MetarRunwayVisualRange]]: A list of `MetarRunwayVisualRange` instances containing runway
             visual range data.
        """
        matches = re.finditer(
            r" R(?P<runway>\w{2,3})/(?P<modifier>\w?)(?P<value>\d{4})(?P<tendency>\w?)",
            self.metar,
        )
        to_return = []
        for match in matches:
            to_return.append(
                MetarRunwayVisualRange(
                    runway=match.group("runway"),
                    modifier=(
                        match.group("modifier") if match.group("modifier") else None
                    ),
                    value=match.group("value"),
                    tendency=(
                        match.group("tendency") if match.group("tendency") else None
                    ),
                )
            )
        return to_return

    def parse_present_weather(self) -> list[Optional[MetarPresentWeather]]:
        """
        Parse present weather conditions data from the raw METAR string.

        Returns:
            list[Optional[MetarPresentWeather]]: A list of `MetarPresentWeather` instances containing present
            weather conditions data.
        """
        matches = re.finditer(
            r"\s(?P<intensity>-|\+|VC)?"
            r"(?P<descriptor>MI|BC|PR|DR|BL|SH|TS|FZ)?"
            r"(?P<precipitation>DZ|RA|SN|SG|PL|GR|GS|UP)?"
            r"(?P<obscuration>BR|FG|FU|VA|DU|SA|HZ)?"
            r"(?P<other>PO|SQ|FC|SS|DS)?",
            self.metar,
        )
        to_return = []
        for match in matches:
            if not [value for value in match.groupdict().values() if value]:
                continue
            to_return.append(MetarPresentWeather(**match.groupdict()))
        return to_return

    def parse_clouds(self) -> list[Optional[MetarClouds]]:
        """
        Parse cloud cover data from the raw METAR string.

        Returns:
            list[Optional[MetarClouds]]: A list of `MetarClouds` instances containing cloud cover data.
        """
        matches = re.finditer(
            r"\s(?P<cloud_amount>FEW|SCT|BKN|OVC)(?P<height_of_base>\d+)?(?P<cloud_type>CB|TCU)?",
            self.metar,
        )
        to_return = []
        for match in matches:
            group_dict = match.groupdict()
            height_of_base = int(group_dict.pop("height_of_base")) * 100
            to_return.append(MetarClouds(**group_dict, height_of_base=height_of_base))
        return to_return

    def parse_vertical_visibility(self) -> int | None:
        """
        Parse vertical visibility data from the raw METAR string.

        Returns:
            int | None: The vertical visibility value in meters or None if not available.
        """
        match = re.search(r"\sVV(?P<visibility>\d{3})", self.metar)
        if not match:
            return None
        return int(match.groupdict().get("visibility")) * 100

    def parse_cavok(self) -> bool:
        """
        Parse CAVOK (Ceiling and Visibility OK) condition from the raw METAR string.

        Returns:
            bool: True if CAVOK condition is present, False otherwise.
        """
        matches = re.search(r"(?P<cavok>CAVOK)", self.metar)
        return bool(matches)

    def parse_air_and_dew_point_temperature(self) -> MetarTemperatures:
        """
        Parse air and dew point temperature data from the raw METAR string.

        Returns:
            MetarTemperatures: An instance of the `MetarTemperatures` class containing temperature data.
        """
        matches = re.search(
            r"(?P<negative_temperature>M)?(?P<temperature>\d{2})\/(?P<negative_dew_point>M)?(?P<dew_point>\d{2})",
            self.metar,
        )
        return MetarTemperatures(
            air_temperature=(
                matches.group("temperature")
                if not matches.group("negative_temperature")
                else -int(matches.group("temperature"))
            ),
            dew_point_temperature=(
                matches.group("dew_point")
                if not matches.group("negative_dew_point")
                else -int(matches.group("dew_point"))
            ),
        )

    def parse_pressure(self) -> int:
        """
        Parse atmospheric pressure data (QNH) from the raw METAR string.

        Returns:
            int: The atmospheric pressure (QNH) value in hectopascals (hPa).
        """
        matches = re.search(r"Q(?P<qnh>\d{4})", self.metar)
        return matches.group("qnh")

    def parse_wind_shear(self) -> list[Optional[WindShear]]:
        """
        Parse wind shear data from the raw METAR string.

        Returns:
            list[Optional[WindShear]]: A list of `WindShear` instances containing wind shear data.
        """
        if "WS ALLRWY" in self.metar:
            return list(WindShear(all_runways=True))
        matches = re.finditer(r"WS R(?P<runway>\d{2})", self.metar)
        return [WindShear(runway=match.group("runway")) for match in matches]

    def parse_trend_forecast(self) -> Optional[str]:
        """
        Parse trend forecast data from the raw METAR string.

        Returns:
            Optional[str]: The trend forecast data as a string, or None if no forecast is available.
        """
        matches = re.finditer(
            r"(?P<trend>TEMPO|BECMG|NOSIG)\s?(?P<content>.*)", self.metar
        )
        trend_forecast = " ".join([" ".join(match.groups()) for match in matches])
        return None if trend_forecast == " " else trend_forecast


def parse_metar(raw_metar: str) -> Metar:
    """
    Parse a raw METAR (Meteorological Aerodrome Report) string into structured data.

    Args:
        raw_metar (str): The raw METAR weather data string to be parsed.

    Returns:
        Metar: An instance of the `Metar` class containing parsed METAR data.
    """
    metar, *remarks = raw_metar.split("RMK ")
    parser = MetarParser(metar=metar)
    return Metar(
        identification=parser.parse_identification(),
        surface_wind=parser.parse_surface_wind(),
        visibility=parser.parse_prevailing_visibility(),
        runway_visual_range=parser.parse_runway_visual_range(),
        present_weather=parser.parse_present_weather(),
        clouds=parser.parse_clouds(),
        vertical_visibility=parser.parse_vertical_visibility(),
        cavok=parser.parse_cavok(),
        temperatures=parser.parse_air_and_dew_point_temperature(),
        pressure=parser.parse_pressure(),
        wind_shear=parser.parse_wind_shear(),
        trend_forecast=parser.parse_trend_forecast(),
        remarks="".join(remarks) if remarks else None,
        raw=raw_metar,
    )
