from typing import Optional

import pytest

from src.api.v1.airport.lowvis import lvo
from src.schemas import MetarRunwayVisualRange, MetarClouds, MetarVisibility


@pytest.mark.parametrize(
    "visibility,runway_visual_range,clouds,expected",
    [
        (
            None,
            [
                MetarRunwayVisualRange(runway="28", value=f"{value}")
                for value in range(0, 801)
            ],
            [],
            True,
        ),
        (
            None,
            [
                MetarRunwayVisualRange(runway="28", value=f"{value}")
                for value in range(801, 9999)
            ],
            [],
            False,
        ),
        (
            None,
            [],
            [
                MetarClouds(cloud_amount="OVC", height_of_base=f"{value}")
                for value in range(0, 201)
            ],
            True,
        ),
        (
            None,
            [],
            [
                MetarClouds(cloud_amount="OVC", height_of_base=f"{value}")
                for value in range(201, 9999)
            ],
            False,
        ),
        (
            None,
            [],
            [
                MetarClouds(cloud_amount="BKN", height_of_base=f"{value}")
                for value in range(0, 201)
            ],
            True,
        ),
        (
            None,
            [],
            [
                MetarClouds(cloud_amount="BKN", height_of_base=f"{value}")
                for value in range(201, 9999)
            ],
            False,
        ),
        (
            None,
            [MetarRunwayVisualRange(runway="28", value=800)],
            [MetarClouds(cloud_amount="OVC", height_of_base=200)],
            True,
        ),
        (
            None,
            [MetarRunwayVisualRange(runway="28", value=800)],
            [MetarClouds(cloud_amount="BKN", height_of_base=200)],
            True,
        ),
        (
            None,
            [MetarRunwayVisualRange(runway="28", value=850)],
            [MetarClouds(cloud_amount="OVC", height_of_base=300)],
            False,
        ),
    ],
)
def test_lpfr(
    visibility: Optional[MetarVisibility],
    runway_visual_range: list[Optional[MetarRunwayVisualRange]],
    clouds: list[Optional[MetarClouds]],
    expected: bool,
):
    """
    Test the logic for determining if low visibility operations (LVO) are required at LPFR airport.

    Args:
        visibility (Optional[MetarVisibility]): The METAR visibility information.
        runway_visual_range (list[Optional[MetarRunwayVisualRange]]): The METAR runway visual range information.
        clouds (list[Optional[MetarClouds]]): The METAR cloud information.
        expected (bool): The expected result of LVO requirement.

    Returns:
        None: This function raises an AssertionError if the result does not match the expected value.
    """
    assert (
        lvo(
            icao="lpfr",
            visibility=visibility,
            runway_visual_range=runway_visual_range,
            clouds=clouds,
        )
        == expected
    )


@pytest.mark.parametrize(
    "visibility,runway_visual_range,clouds,expected",
    [
        (
            None,
            [
                MetarRunwayVisualRange(runway="17", value=f"{value}")
                for value in range(0, 551)
            ],
            [],
            True,
        ),
        (
            None,
            [
                MetarRunwayVisualRange(runway="17", value=f"{value}")
                for value in range(551, 9999)
            ],
            [],
            False,
        ),
        (
            None,
            [],
            [
                MetarClouds(cloud_amount="OVC", height_of_base=f"{value}")
                for value in range(0, 300)
            ],
            True,
        ),
        (
            None,
            [],
            [
                MetarClouds(cloud_amount="OVC", height_of_base=f"{value}")
                for value in range(300, 9999)
            ],
            False,
        ),
        (
            None,
            [],
            [
                MetarClouds(cloud_amount="BKN", height_of_base=f"{value}")
                for value in range(0, 300)
            ],
            True,
        ),
        (
            None,
            [],
            [
                MetarClouds(cloud_amount="BKN", height_of_base=f"{value}")
                for value in range(300, 9999)
            ],
            False,
        ),
        (
            None,
            [MetarRunwayVisualRange(runway="17", value=550)],
            [MetarClouds(cloud_amount="OVC", height_of_base=400)],
            True,
        ),
        (
            None,
            [MetarRunwayVisualRange(runway="17", value=550)],
            [MetarClouds(cloud_amount="BKN", height_of_base=400)],
            True,
        ),
        (
            None,
            [MetarRunwayVisualRange(runway="17", value=850)],
            [MetarClouds(cloud_amount="OVC", height_of_base=500)],
            False,
        ),
    ],
)
def test_lppr(
    visibility: Optional[MetarVisibility],
    runway_visual_range: list[Optional[MetarRunwayVisualRange]],
    clouds: list[Optional[MetarClouds]],
    expected: bool,
):
    """
    Test the logic for determining if low visibility operations (LVO) are required at LPPR airport.

    Args:
        visibility (Optional[MetarVisibility]): The METAR visibility information.
        runway_visual_range (list[Optional[MetarRunwayVisualRange]]): The METAR runway visual range information.
        clouds (list[Optional[MetarClouds]]): The METAR cloud information.
        expected (bool): The expected result of LVO requirement.

    Returns:
        None: This function raises an AssertionError if the result does not match the expected value.
    """
    assert (
        lvo(
            icao="lppr",
            visibility=visibility,
            runway_visual_range=runway_visual_range,
            clouds=clouds,
        )
        == expected
    )


@pytest.mark.parametrize(
    "visibility,runway_visual_range,clouds,expected",
    [
        (
            None,
            [
                MetarRunwayVisualRange(runway="20", value=f"{value}")
                for value in range(0, 551)
            ],
            [],
            True,
        ),
        (
            None,
            [
                MetarRunwayVisualRange(runway="20", value=f"{value}")
                for value in range(551, 9999)
            ],
            [],
            False,
        ),
        (
            None,
            [
                MetarRunwayVisualRange(runway="02", value=f"{value}")
                for value in range(0, 801)
            ],
            [],
            True,
        ),
        (
            None,
            [
                MetarRunwayVisualRange(runway="02", value=f"{value}")
                for value in range(801, 9999)
            ],
            [],
            False,
        ),
        (
            None,
            [],
            [
                MetarClouds(cloud_amount="OVC", height_of_base=f"{value}")
                for value in range(0, 201)
            ],
            True,
        ),
        (
            None,
            [],
            [
                MetarClouds(cloud_amount="OVC", height_of_base=f"{value}")
                for value in range(201, 9999)
            ],
            False,
        ),
        (
            None,
            [],
            [
                MetarClouds(cloud_amount="BKN", height_of_base=f"{value}")
                for value in range(0, 201)
            ],
            True,
        ),
        (
            None,
            [],
            [
                MetarClouds(cloud_amount="BKN", height_of_base=f"{value}")
                for value in range(201, 9999)
            ],
            False,
        ),
        (
            None,
            [MetarRunwayVisualRange(runway="20", value=550)],
            [MetarClouds(cloud_amount="OVC", height_of_base=200)],
            True,
        ),
        (
            None,
            [MetarRunwayVisualRange(runway="02", value=800)],
            [MetarClouds(cloud_amount="OVC", height_of_base=200)],
            True,
        ),
        (
            None,
            [MetarRunwayVisualRange(runway="02", value=800)],
            [MetarClouds(cloud_amount="BKN", height_of_base=200)],
            True,
        ),
        (
            None,
            [MetarRunwayVisualRange(runway="02", value=850)],
            [MetarClouds(cloud_amount="OVC", height_of_base=300)],
            False,
        ),
    ],
)
def test_lppt(
    visibility: Optional[MetarVisibility],
    runway_visual_range: list[Optional[MetarRunwayVisualRange]],
    clouds: list[Optional[MetarClouds]],
    expected: bool,
):
    """
    Test the logic for determining if low visibility operations (LVO) are required at LPPT airport.

    Args:
        visibility (Optional[MetarVisibility]): The METAR visibility information.
        runway_visual_range (list[Optional[MetarRunwayVisualRange]]): The METAR runway visual range information.
        clouds (list[Optional[MetarClouds]]): The METAR cloud information.
        expected (bool): The expected result of LVO requirement.

    Returns:
        None: This function raises an AssertionError if the result does not match the expected value.
    """
    assert (
        lvo(
            icao="lppt",
            visibility=visibility,
            runway_visual_range=runway_visual_range,
            clouds=clouds,
        )
        == expected
    )


@pytest.mark.parametrize(
    "visibility,runway_visual_range,clouds,expected",
    [
        (
            None,
            [],
            [
                MetarClouds(cloud_amount="OVC", height_of_base=f"{value}")
                for value in range(0, 801)
            ],
            True,
        ),
        (
            None,
            [],
            [
                MetarClouds(cloud_amount="OVC", height_of_base=f"{value}")
                for value in range(801, 9999)
            ],
            False,
        ),
        (
            None,
            [],
            [
                MetarClouds(cloud_amount="BKN", height_of_base=f"{value}")
                for value in range(0, 801)
            ],
            True,
        ),
        (
            None,
            [],
            [
                MetarClouds(cloud_amount="BKN", height_of_base=f"{value}")
                for value in range(801, 9999)
            ],
            False,
        ),
        (MetarVisibility(prevailing_visibility=0), [], [], True),
        (MetarVisibility(prevailing_visibility=4999), [], [], True),
        (MetarVisibility(prevailing_visibility=5000), [], [], True),
        (MetarVisibility(prevailing_visibility=5001), [], [], False),
        (MetarVisibility(prevailing_visibility=9999), [], [], False),
        (
            MetarVisibility(prevailing_visibility=5000),
            [],
            [MetarClouds(cloud_amount="OVC", height_of_base=400)],
            True,
        ),
        (
            MetarVisibility(prevailing_visibility=5000),
            [],
            [MetarClouds(cloud_amount="BKN", height_of_base=400)],
            True,
        ),
        (
            MetarVisibility(prevailing_visibility=9999),
            [],
            [MetarClouds(cloud_amount="OVC", height_of_base=1600)],
            False,
        ),
    ],
)
def test_lpma(
    visibility: Optional[MetarVisibility],
    runway_visual_range: list[Optional[MetarRunwayVisualRange]],
    clouds: list[Optional[MetarClouds]],
    expected: bool,
):
    """
    Test the logic for determining if low visibility operations (LVO) are required at LPMA airport.

    Args:
        visibility (Optional[MetarVisibility]): The METAR visibility information.
        runway_visual_range (list[Optional[MetarRunwayVisualRange]]): The METAR runway visual range information.
        clouds (list[Optional[MetarClouds]]): The METAR cloud information.
        expected (bool): The expected result of LVO requirement.

    Returns:
        None: This function raises an AssertionError if the result does not match the expected value.
    """
    assert (
        lvo(
            icao="lpma",
            visibility=visibility,
            runway_visual_range=runway_visual_range,
            clouds=clouds,
        )
        == expected
    )
