import pytest

from src.metar_parser import MetarParser
from src.schemas import MetarRunwayVisualRange, MetarPresentWeather, MetarClouds


@pytest.mark.parametrize(
    "metar,expected",
    [
        (
            "LOWL 141320Z VRB02KT 0500 R26/0900U R27L/M0050U FG VV001 M00/M00 Q1035",
            [
                MetarRunwayVisualRange(
                    runway="26", modifier=None, value=900, tendency="U"
                ),
                MetarRunwayVisualRange(
                    runway="27L", modifier="M", value=50, tendency="U"
                ),
            ],
        ),
        (
            "LOWL 141320Z VRB02KT 0500 R22/5900N FG VV001 M00/M00 Q1035",
            [
                MetarRunwayVisualRange(
                    runway="22", modifier=None, value=5900, tendency="N"
                )
            ],
        ),
        ("LOWL 141320Z VRB02KT 0500 FG VV001 M00/M00 Q1035", []),
    ],
)
def test_parse_runway_visual_range(metar: str, expected: list):
    """
    Test the parsing of runway visual range information from a METAR string.

    Args:
        metar (str): The METAR string to be parsed.
        expected (list): The expected list of MetarRunwayVisualRange objects extracted from the METAR.

    Returns:
        None: This function raises an AssertionError if the parsed result does not match the expected output.
    """
    parser = MetarParser(metar=metar)
    assert parser.parse_runway_visual_range() == expected


@pytest.mark.parametrize(
    "metar,expected",
    [
        (
            "LOWL 141320Z VRB02KT 0500 FG +SHRA VV001 M00/M00 Q1035 TEMPO 1200 BCFG OVC002",
            [
                MetarPresentWeather(
                    intensity=None,
                    descriptor=None,
                    precipitation=None,
                    obscuration="FG",
                    other=None,
                ),
                MetarPresentWeather(
                    intensity="+",
                    descriptor="SH",
                    precipitation="RA",
                    obscuration=None,
                    other=None,
                ),
                MetarPresentWeather(
                    intensity=None,
                    descriptor="BC",
                    precipitation=None,
                    obscuration="FG",
                    other=None,
                ),
            ],
        ),
    ],
)
def test_parse_present_weather(metar: str, expected: list):
    """
    Test the parsing of present weather information from a METAR string.

    Args:
        metar (str): The METAR string to be parsed.
        expected (list): The expected list of MetarPresentWeather objects extracted from the METAR.

    Returns:
        None: This function raises an AssertionError if the parsed result does not match the expected output.
    """
    parser = MetarParser(metar=metar)
    assert parser.parse_present_weather() == expected


@pytest.mark.parametrize(
    "metar,expected",
    [
        (
            "LOWL 141320Z VRB02KT 0500 FG +SHRA VV001 OVC002 M00/M00 Q1035",
            [MetarClouds(cloud_amount="OVC", height_of_base=200, cloud_type=None)],
        ),
        (
            "LIRP 142215Z VRB01KT 0200 R04R/0325 FG BKN000 03/03 Q1032 NOSIG",
            [MetarClouds(cloud_amount="BKN", height_of_base=0, cloud_type=None)],
        ),
        (
            "LIRP 142215Z VRB01KT 0200 R04R/0325 FG BKN000CB 03/03 Q1032 NOSIG",
            [MetarClouds(cloud_amount="BKN", height_of_base=0, cloud_type="CB")],
        ),
    ],
)
def test_parse_clouds(metar: str, expected: list):
    """
    Test the parsing of cloud information from a METAR string.

    Args:
        metar (str): The METAR string to be parsed.
        expected (list): The expected list of MetarClouds objects extracted from the METAR.

    Returns:
        None: This function raises an AssertionError if the parsed result does not match the expected output.
    """
    parser = MetarParser(metar=metar)
    assert parser.parse_clouds() == expected
