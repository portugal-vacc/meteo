fastapi==0.92.0
httpx==0.23.3
pytest==7.2.1
coverage==7.2.0
pydantic==1.10.5
hypercorn==0.14.3
python-dotenv==1.0.0